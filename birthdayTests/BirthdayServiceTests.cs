﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using birthdays;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace birthdayTests
{
    [TestClass]
    public class BirthdayServiceTests
    {
        private Mock<IEmployeeRepository> employeeRepositoryMock;
        private Mock<IEmailService> emailServiceMock;
        private BirthdayService birthdayService;

        [TestInitialize]
        public void InitializeInterfaces()
        {
            employeeRepositoryMock =
                new Mock<IEmployeeRepository>();
            
            emailServiceMock =
                new Mock<IEmailService>();

            birthdayService = new BirthdayService(
                employeeRepositoryMock.Object, 
                emailServiceMock.Object);
        }

        [TestMethod]
        public void SendGreetingsEmailSentTest()
        {
            var employees = new List<Employee>()
            {
                new Employee(),
                new Employee(),
                new Employee()
            };

            employeeRepositoryMock
                .Setup(f => f.GetEmployeesByBirthday(DateTime.Today))
                .Returns(employees);

            emailServiceMock
                .Setup(f => f.SendEmail(
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>()))
                .Verifiable();

            birthdayService.sendGreetings(DateTime.Today);

            emailServiceMock.Verify(
                f => f.SendEmail(
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>())
               , Times.Exactly(3));
        }

        [TestMethod]
        public void CreateEmailBodyTest()
        {
            var employee1 = new Employee
            {
                FirstName = "Bob"
            };

            var result = birthdayService.createEmailBody(employee1);

            Assert.IsTrue(result.Contains(employee1.FirstName));
        }
    }
}
