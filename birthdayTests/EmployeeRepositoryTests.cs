﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using birthdays;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace birthdayTests
{
    [TestClass]
    public class EmployeeRepositoryTests
    {
        [TestMethod]
        public void GetEmployeesByBirthdayTest()
        {
            var employees = new List<Employee>
            {
                new Employee {Birthday = new DateTime(2014, 1, 29)},
                new Employee {Birthday = new DateTime(2014, 1, 29)},
                new Employee {Birthday = new DateTime(2014, 2, 26)},
                new Employee {Birthday = new DateTime(2014, 4, 29)},
                new Employee {Birthday = new DateTime(2014, 5, 29)},
                new Employee {Birthday = new DateTime(2012, 1, 29)}
            };

            var employeeRepository = 
                new Mock<EmployeeRepository>
                { CallBase = true };

            employeeRepository
                .Setup(f => f.GetEmployees())
                .Returns(employees);

            var result = employeeRepository.Object.GetEmployeesByBirthday(new DateTime(2014, 1, 29));

            Assert.AreEqual(3, result.Count());
        }
    }
}
