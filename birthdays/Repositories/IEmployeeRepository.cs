﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace birthdays
{
    public interface IEmployeeRepository
    {
        List<Employee> GetEmployees();
        List<Employee> GetEmployeesByBirthday(DateTime birthday);
    }
}
