﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace birthdays
{
    public class EmployeeRepository : IEmployeeRepository
    {
        public virtual List<Employee> GetEmployees()
        {
            throw new NotImplementedException();
        }

        public List<Employee> GetEmployeesByBirthday(DateTime birthday)
        {
            return GetEmployees()
                .Where(x => x.Birthday.Day == birthday.Day && x.Birthday.Month == birthday.Month)
                .ToList();
        }
    }
}
