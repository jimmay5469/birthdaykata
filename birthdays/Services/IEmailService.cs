﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace birthdays
{
    public interface IEmailService
    {
        void SendEmail(
            string ToAddress, 
            string Subject, 
            string Body);
    }
}
