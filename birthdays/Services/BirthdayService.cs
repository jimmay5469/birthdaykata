﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace birthdays
{
    public class BirthdayService
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IEmailService _emailService;

        public BirthdayService(
            IEmployeeRepository employeeRepository, 
            IEmailService emailService)
        {
            _employeeRepository = employeeRepository;
            _emailService = emailService;
        }

        public void sendGreetings(DateTime today)
        {
            foreach (var employee in _employeeRepository.GetEmployeesByBirthday(today))
            {
                _emailService.SendEmail(
                    employee.Email,
                    "Happy Birthday",
                    createEmailBody(employee)
                    );
            }
        }

        public string createEmailBody(Employee employee)
        {
            return string.Format("Happy birthday, dear {0}!", employee.FirstName);
        }
    }
}
